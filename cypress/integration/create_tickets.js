/// <reference types="cypress" />

import { Ticket } from "../page_objects/ticket.object"

let i = 15
//Cypress._.times(100, () => {

  for(let i=250 ; i<500;i++){
  //let i = 15
  describe('Auto create ticket number: '+i, () => {
    const ticket = new Ticket
    it('Tiket number '+i+' is created sacessfully?', async () => {
        await cy.visit('https://www.dev.unifai.de/')
        await cy.get('#mat-input-0').type('cascade2')
        await cy.get('#mat-input-1').type('Pass123!')
        await cy.get('.mat-button-wrapper').click()
        await cy.wait(4000)
        await cy.get(':nth-child(5) > .mat-focus-indicator > .mat-button-wrapper > .svg-icon-wrapper').click()
        await cy.get('[style="display: flex; justify-content: center; flex-direction: column;"] > .mat-icon-button > .mat-button-wrapper > .mat-icon').click()
        await cy.get('#mat-input-2').type(i+'.tiket')
        await cy.get('[style="display: flex; flex-direction: column;"] > :nth-child(2) > div > .mat-menu-trigger > span').click()
        await cy.get('.mat-menu-content > .mat-focus-indicator').click()
        await cy.get('button').contains('Room 101').click()
        await cy.get('#mat-chip-list-4 > .mat-chip-list-wrapper > .mat-chip > .poppins-14').click()
        await cy.get('.mat-menu-content > :nth-child(2) > span').click()
        await cy.wait(2000)
        await cy.get('button').contains('CREATE TICKET').click()
        await cy.wait(3000)
    })
  })
}

//})
