/// <reference types="cypress" />

import { User } from "../page_objects/user.object"


for(let i=500 ; i<1000;i++){

  describe('Auto create user number: '+i, () => {
    const user = new User
    it('User number '+i+' is created sacessfully?', async () => {
        await cy.visit('https://www.dev.unifai.de/')
        await cy.get('#mat-input-0').type('cascade2')
        await cy.get('#mat-input-1').type('Pass123!')
        await cy.get('.mat-button-wrapper').click()
        await cy.wait(4000)
        await cy.get(':nth-child(1) > .mat-focus-indicator > .mat-button-wrapper > .circular-button').click()
        await cy.get('.mat-tooltip-trigger > span > .mat-icon').click()
        await cy.get('[routerlink="../employees"]').click()
        await cy.wait(2000)
        await cy.get('.left-side > :nth-child(3) > .mat-focus-indicator > .mat-button-wrapper').click()
        await cy.wait(2000)
        await cy.get('#mat-input-8').type('cascade2admin'+i)
        await cy.get('#mat-input-9').type('Admin '+i)
        await cy.get('#mat-input-10').type('Cascade 2')
        await cy.get('#mat-input-11').type('mail@gmail.com')
        await cy.get('.mat-select-placeholder').type('{enter}')
        await cy.wait(1000)
        await cy.wait(1000)
        //await cy.get('span').contains('Admin').click()
        cy.get('.flex > .mat-focus-indicator > .mat-button-wrapper').type('{enter}')
        await cy.wait(5000)
    })
  })
}
  
