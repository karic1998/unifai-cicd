/// <reference types="cypress" />


export class User {


    async create_user(i){
        await cy.get(':nth-child(1) > .mat-focus-indicator > .mat-button-wrapper > .circular-button').click()
        await cy.get('.mat-tooltip-trigger > span > .mat-icon').click()
        await cy.get('[routerlink="../employees"]').click()
        await cy.get('.left-side > :nth-child(3) > .mat-focus-indicator > .mat-button-wrapper').click()
        await cy.get('#mat-input-8').type('cascade2admin'+i)
        await cy.get('#mat-input-9').type('Admin '+i)
        await cy.get('#mat-input-10').type('Cascade 2')
        await cy.get('#mat-input-11').type('mail@gmail.com')
        await cy.get('.mat-select-placeholder').click()
        await cy.get('#mat-option-204 > .mat-option-text').click()
        await cy.get('.flex > .mat-focus-indicator').click()
    }
}